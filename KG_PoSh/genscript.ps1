﻿
function New-IcsEvent
{
    [OutputType([System.IO.FileInfo[]])]
    [CmdletBinding(SupportsShouldProcess, ConfirmImpact = 'Low')]
    param
    (
        [Parameter(Mandatory, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [datetime[]]
        $StartDate,

        [Parameter(Mandatory)]
        [string]
        $Path,

        [Parameter(Mandatory)]
        [string]
        $CalendarName,

        [Parameter(Mandatory)]
        [string]
        $Subject,

        [Parameter()]
        [string]
        $Location,

        [Parameter()]
        [bool]
        $AllDay = 0,

        [Parameter()]
        [timespan]
        $Duration = '1:00:00',

        [Parameter()]
        [string]
        $EventDescription,

        [Parameter()]
        [switch]
        $PassThru,

        [ValidateSet('Free', 'Busy')]
        [string]
        $ShowAs = 'Busy',

        [ValidateSet('Private', 'Public', 'Confidential')]
        [string]
        $Visibility = 'Private',

        [string[]]
        $Category
    )

    begin
    {
        # Custom date formats that we want to use
        if ($AllDay) {
            $icsDateFormat = "yyyyMMdd"
        } else {
            $icsDateFormat = "yyyyMMddTHHmmssZ"
        }
    }

    process
    {
        # Checkout RFC 5545 for more options
        $fileName = Join-Path -Path $Path -ChildPath "$CalendarName.ics"
        $calendar = @"
BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
PRODID:-//BS//Hobby Code//EN
"@
        $events = @()
        foreach ($Date in $StartDate)
        {
            if ($AllDay) {
                $DtStart = "; VALUE=DATE:$($Date.ToUniversalTime().ToString($icsDateFormat))"
                $DtEnd = "; VALUE=DATE:$($Date.Add($Duration).ToUniversalTime().ToString($icsDateFormat))"
            } else {
                $DtStart = ": $($Date.ToUniversalTime().ToString($icsDateFormat))"
                $DtEnd = ": $($Date.Add($Duration).ToUniversalTime().ToString($icsDateFormat))"
            }
            
            $event += @"

BEGIN:VEVENT
UID: $([guid]::NewGuid())
CREATED: $((Get-Date).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
DTSTAMP: $((Get-Date).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
LAST-MODIFIED: $((Get-Date).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
CLASS:$Visibility
CATEGORIES:$($Category -join ',')
SEQUENCE:0
DTSTART$DtStart
DTEND$DtEnd
DESCRIPTION: $EventDescription
SUMMARY: $Subject
LOCATION: $Location
TRANSP:$(if($ShowAs -eq 'Free') {'TRANSPARENT'})
END:VEVENT
"@
            $events += $event
        }
        $calendar += $events -join "`n"
        $calendar += @"

END:VCALENDAR
"@
        if ($PSCmdlet.ShouldProcess($fileName, 'Write ICS file'))
        {
            $calendar | Out-File -FilePath $fileName -Encoding utf8 -Force
            if ($PassThru) { Get-Item -Path $fileName }
        }
    }
}

$FindNthDay=2
$WeekDay='Tuesday'
$MonthNo=1
$MinYear=2021
$Date= Get-Date -Date "$MinYear-$MonthNo-1"

while ($Date.DayofWeek -ine $WeekDay ) { $Date=$Date.AddDays(1) }
$Date = $Date.AddDays(7*($FindNthDay-1))

$Date | New-IcsEvent -Path D:\temp -CalendarName modo -Subject 'Testafspraak' -AllDay 1
